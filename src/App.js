import React, { Suspense, lazy, useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { MuiThemeProvider } from "@material-ui/core/styles";

import { history } from "./store";
import routes from "./routes";
import { ProtectedRoute } from "./containers/Routers/components/ProtectedRoute";
import { Providers } from "./componets/Provider/Provider";
import { getToken } from "./store/selectors/login";
import { retrieveUrlPromisify } from "./store/actions/retrieveUrl";
import { Notifications } from "./componets/Notification/Notifications";

import { theme } from "./theme";

const Login = lazy(() => import("./containers/Login"));
const ContentPage = lazy(() => import("./containers/ContentPage"));

function App({ token, retrieveUrlPromisify }) {
  useEffect(() => {
    if (token) {
      retrieveUrlPromisify(token);
    }
  });

  return (
    <MuiThemeProvider theme={theme}>
      <ConnectedRouter history={history}>
        <Providers>
          <Suspense fallback={<h1>Loading...</h1>}>
            <Switch>
              <Route exact path={routes.login} component={Login} permitted={!!token} />
              <ProtectedRoute
                exact
                path={routes.contentPage}
                component={ContentPage}
                permitted={!!token}
              />
              <Redirect to={routes.login} />
            </Switch>
          </Suspense>
          <Notifications />
        </Providers>
      </ConnectedRouter>
    </MuiThemeProvider>
  );
}

export default connect(
  (state) => ({
    token: getToken(state),
  }),
  { retrieveUrlPromisify }
)(App);
