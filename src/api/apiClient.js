import httpClient from "./axiosConfig";
import { buildUrl } from "../utils/buildUrl";

export const authentication = {
  login: (data) =>
    httpClient.request({
      url: buildUrl("login"),
      method: "POST",
      data,
    }),
};

export const retrieveUrl = {
  subscribe: (token) =>
    httpClient.request({
      url: buildUrl("subscribe"),
      method: "GET",
      headers: { "x-test-app-jwt-token": token },
    }),
};
