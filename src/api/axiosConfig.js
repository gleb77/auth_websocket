import axios from "axios";

import { NotificationsManager } from "../componets/Notification/Notifications";

const apiClient = axios.create({
  baseURL: process.env.REACT_APP_API_PATH,
});

apiClient.interceptors.request.use((config) => ({
  ...config,
  headers: config.headers,
}));

apiClient.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    let notificationOptions = {};
    const {
      status,
      data: { code, description },
    } = error.response;

    if (status === 401) {
      error.response.unauthorized = status;
    }

    notificationOptions = { title: code, message: description };

    if (notificationOptions && NotificationsManager.add) {
      NotificationsManager.add(notificationOptions);
    }

    return Promise.reject(error.response);
  }
);

export default apiClient;
