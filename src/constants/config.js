export const endpoints = {
  baseUrl: "https://work.vint-x.net",
  login: "/api/login",
  subscribe: "/api/subscribe",
};
