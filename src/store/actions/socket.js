export const EMIT_SOCKET_ACTION = "EMIT_SOCKET_ACTION";
export const EMIT_SOCKET_CONNECTION = "EMIT_SOCKET_CONNECTION";
export const CLOSE_CONNECTION = "CLOSE_CONNECTION";
export const RECEIVE_MESSAGE = "RECEIVE_MESSAGE";
export const RENDER_MESSAGE = "RENDER_MESSAGE";

export const emitConnectionToken = (data) => ({
  type: EMIT_SOCKET_CONNECTION,
  data,
});

export const wrapSocketAction = (action) => ({
  type: EMIT_SOCKET_ACTION,
  action,
});

export const closeConnection = () => ({
  type: CLOSE_CONNECTION,
});

export const receiveMessage = ({ data }) => ({
  type: RECEIVE_MESSAGE,
  message: Object.values(JSON.parse(data))[0],
});

export const renderMessage = (time) => ({
  type: RENDER_MESSAGE,
  message: time,
});
