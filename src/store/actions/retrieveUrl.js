import { ERROR_ACTION, WAIT_FOR_ACTION } from "redux-wait-for-action";

export const REQUEST_RETRIEVE_URL = "REQUEST_RETRIEVE_URL";
export const SUCCESS_RETRIEVE_URL = "SUCCESS_RETRIEVE_URL";
export const ERROR_RETRIEVE_URL = "ERROR_RETRIEVE_URL";

export const retrieveUrlPromisify = (response) => ({
  type: REQUEST_RETRIEVE_URL,
  [WAIT_FOR_ACTION]: SUCCESS_RETRIEVE_URL,
  [ERROR_ACTION]: ERROR_RETRIEVE_URL,
  payload: response,
});

export const retrieveUrlSuccess = (response) => ({
  type: SUCCESS_RETRIEVE_URL,
  payload: response,
});

export const retrieveUrlError = (error) => ({
  type: ERROR_RETRIEVE_URL,
  error,
});
