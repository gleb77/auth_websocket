import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import login from "./loginForm";
import retrieveUrl from "./retrieveUrl";
import socket from "./socket";

const reducers = (history) =>
  combineReducers({
    router: connectRouter(history),
    login,
    retrieveUrl,
    socket,
  });

export default reducers;
