import { LOGIN_INFO_FORM_SUCCESS, LOGOUT } from "../actions/loginForm";
import { handleActions } from "../../utils/redux-actions";

export const initialState = {
  token: null,
};

export default handleActions(
  {
    [LOGIN_INFO_FORM_SUCCESS]: (state, { payload }) => ({
      ...state,
      token: payload.token,
    }),
    [LOGOUT]: () => initialState,
  },
  initialState
);
