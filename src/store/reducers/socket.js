import { handleActions } from "../../utils/redux-actions";
import { RENDER_MESSAGE } from "../actions/socket";

export const initialState = {
  message: null,
};

export default handleActions(
  {
    [RENDER_MESSAGE]: (state, { message }) => ({
      ...state,
      message,
    }),
  },
  initialState
);
