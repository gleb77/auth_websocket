import { SUCCESS_RETRIEVE_URL } from "../actions/retrieveUrl";
import { handleActions } from "../../utils/redux-actions";

export const initialState = {
  url: null,
};

export default handleActions(
  {
    [SUCCESS_RETRIEVE_URL]: (state, { payload }) => ({
      ...state,
      url: payload.url,
    }),
  },
  initialState
);
