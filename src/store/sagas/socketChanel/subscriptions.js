import { closeConnection, receiveMessage } from "../../actions/socket";

export const subscriptions = (emit) => [
  ["close", () => emit(closeConnection())],
  ["message", (data) => emit(receiveMessage(data))],
  ["error", (error) => console.log(error)],
];
