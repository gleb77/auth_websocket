import { take, put, call, all, select, takeLatest } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { createWebSocketConnection } from "./socketConnection";
import { getUrl } from "../../selectors/retrieveUrl";
import { subscriptions } from "./subscriptions";
import { getToken } from "../../selectors/login";
import {
  EMIT_SOCKET_ACTION,
  CLOSE_CONNECTION,
  renderMessage,
} from "../../actions/socket";
import { LOGOUT } from "../../actions/loginForm";
import { retrieveUrlPromisify } from "../../actions/retrieveUrl";
import { timeConverter } from "../../../utils/timeConverter";
let socket;
function createSocketChannel(socket) {
  return eventChannel((emit) => {
    const subscriptionList = subscriptions(emit);
    subscriptionList.forEach((listener) =>
      socket.addEventListener(...listener)
    );

    const unsubscribe = () => {
      subscriptionList.forEach((listener) =>
        socket.removeEventListener(...listener)
      );
    };

    return unsubscribe;
  });
}

export function* watchSocketChanel() {
  const url = yield select(getUrl);
  const token = yield select(getToken);
  socket = yield call(createWebSocketConnection, url);
  const socketChannel = yield call(createSocketChannel, socket);

  while (true) {
    try {
      const { message, type } = yield take(socketChannel);
      yield put(renderMessage(timeConverter(message)));
      if (type === CLOSE_CONNECTION) {
        yield put(retrieveUrlPromisify(token));
      }
    } catch (err) {
      console.error("socket error:", err);
    }
  }
}
export function* watchSocketCloseChanel() {
  try {
    if (socket && socket.close) {
      yield socket.close();
    }
  } catch (err) {
    console.error("socket error:", err);
  }
}

export default function* socketWrapper() {
  yield all([
    takeLatest(EMIT_SOCKET_ACTION, watchSocketChanel),
    takeLatest(LOGOUT, watchSocketCloseChanel),
  ]);
}
