import { all, call, put, takeEvery } from "redux-saga/effects";

import {
  REQUEST_RETRIEVE_URL,
  retrieveUrlError,
  retrieveUrlSuccess,
} from "../actions/retrieveUrl";
import { wrapSocketAction, emitConnectionToken } from "../actions/socket";

import { retrieveUrl } from "../../api/apiClient";

function* retrieveUrlSaga({ payload }) {
  try {
    const { data } = yield call(retrieveUrl.subscribe, payload);
    yield put(retrieveUrlSuccess({ url: data.url }));
    yield put(wrapSocketAction(emitConnectionToken({ url: data.url })));
  } catch (error) {
    yield put(retrieveUrlError(error));
  }
}

export default function* retrieveUrlSagaWrap() {
  yield all([takeEvery(REQUEST_RETRIEVE_URL, retrieveUrlSaga)]);
}
