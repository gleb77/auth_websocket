import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  loginInfoFormSuccess,
  LOGIN_INFO_FORM,
  loginInfoFormError,
  logout,
} from "../actions/loginForm";

import { authentication } from "../../api/apiClient";

export function* loginFormSaga({ payload }) {
  try {
    const { headers } = yield call(authentication.login, payload);
    const token = headers["x-test-app-jwt-token"];
    yield put(loginInfoFormSuccess({ token }));
  } catch (error) {
    if (error.unauthorized) {
      yield put(logout());
    }
    yield put(loginInfoFormError(error));
  }
}

export default function* loginInfoFormSaga() {
  yield all([takeLatest(LOGIN_INFO_FORM, loginFormSaga)]);
}
