import { all, fork } from "redux-saga/effects";

import socketChanel from "./socketChanel";
import retrieveUrl from "./retrieveUrl";
import loginForm from "./loginForm";

export default function* () {
  yield all([fork(loginForm), fork(retrieveUrl), fork(socketChanel)]);
}
