import React from "react";
import { Redirect, Route } from "react-router-dom";

import { ErrorBoundary } from "./ErrorBoundary";
import routes from "../../../routes";

export const ProtectedRoute = ({
  component: PrivateComponent,
  permitted,
  render,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      permitted ? (
        <ErrorBoundary>
          {PrivateComponent ? <PrivateComponent {...props} /> : render(props)}
        </ErrorBoundary>
      ) : (
        <Redirect to={routes.login} />
      )
    }
  />
);
