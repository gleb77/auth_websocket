import React from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";

import { ErrorBoundary } from "../../Routers/components/ErrorBoundary";
import { AutoSaveField as Field } from "../../../componets/Form/AutoSaveField/AutoSaveField";
import { Input } from "../../../componets/Form/Input/Input";
import { SubmitButton } from "../../../componets/Buttons/SubmitButton";
import { USER_NAME_REGEX, PASSWORD_REGEX } from "../../../utils/validation";
import {
  getInvalidMessage,
  getRequiredMessage,
} from "../../../utils/getValidationMessage";

import { useStyles } from "./styled";

const INITIAL_VALUES = { userName: "", password: "" };

const loginSchema = Yup.object({
  userName: Yup.string()
    .required(getRequiredMessage("User name"))
    .matches(USER_NAME_REGEX, getInvalidMessage("User name")),
  password: Yup.string()
    .required(getRequiredMessage("Password"))
    .matches(PASSWORD_REGEX, getInvalidMessage("Password")),
});

export const LoginComponent = ({ submitForm, isLoading }) => {
  const classes = useStyles();

  return (
    <ErrorBoundary>
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <Formik
            initialValues={INITIAL_VALUES}
            validationSchema={loginSchema}
            validateOnChange={false}
            onSubmit={submitForm}
          >
            {({ values }) => (
              <Form className={classes.form}>
                <Field
                  name="userName"
                  label="User Name"
                  placeholder="User Name"
                  component={Input}
                />
                <Field
                  name="password"
                  type="password"
                  label="Your Password"
                  placeholder="Your Password"
                  component={Input}
                />
                <SubmitButton
                  label="Submit"
                  isDisplayLoader={isLoading}
                  disabled={!values.userName || !values.password || isLoading}
                />
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </ErrorBoundary>
  );
};
