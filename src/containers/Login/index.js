import { connect } from "react-redux";

import { loginInfoFormPromisify } from "../../store/actions/loginForm";
import { getToken } from "../../store/selectors/login";
import { Login } from "./Login";

export default connect(
  (state) => ({
    token: getToken(state),
  }),
  {
    login: loginInfoFormPromisify,
  }
)(Login);
