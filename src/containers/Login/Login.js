import React, { useCallback, useState, useEffect } from "react";

import { LoginComponent } from "./components/Login";
import routes from "../../routes";

export const Login = ({ login, history, token }) => {
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (token) {
      history.push(routes.contentPage);
    }
  }, [history, token]);

  const submitForm = useCallback(
    (values) => {
      setIsLoading(true);

      return login(values).then(
        () => {
          setIsLoading(false);
          history.push(routes.contentPage);
        },
        () => {
          setIsLoading(false);
        }
      );
    },
    [login, history]
  );

  return <LoginComponent isLoading={isLoading} submitForm={submitForm} />;
};
