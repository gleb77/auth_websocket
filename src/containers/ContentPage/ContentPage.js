import React from "react";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import PageviewIcon from "@material-ui/icons/Pageview";
import Typography from "@material-ui/core/Typography";
import { TimeFromServer } from "../ContentPage/components/TimeFromServer";

import { useStyles } from "./styled";
import { SubmitButton } from "../../componets/Buttons/SubmitButton";

export const ContentPage = ({ message, logout }) => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <PageviewIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Content Page
        </Typography>
        <TimeFromServer message={message} />
        <SubmitButton label="Logout" onClick={logout} />
      </div>
    </Container>
  );
};
