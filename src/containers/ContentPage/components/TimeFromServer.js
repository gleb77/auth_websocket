import React from "react";

import { useStyles } from "./styled";

export const TimeFromServer = ({ message }) => {
  const classes = useStyles();

  return (
    <div className={classes.timeServer}>
      <span>{message}</span>
    </div>
  );
};
