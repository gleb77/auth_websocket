import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
  timeServer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    minHeight: "100px",
    justifyContent: "center",
    "& span": {
      fontSize: "30px",
    },
  },
});
