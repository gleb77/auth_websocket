import { connect } from "react-redux";

import { logout } from "../../store/actions/loginForm";
import { getMessage } from "../../store/selectors/socket";
import { ContentPage } from "./ContentPage";

export default connect(
  (state) => ({
    message: getMessage(state),
  }),
  { logout }
)(ContentPage);
