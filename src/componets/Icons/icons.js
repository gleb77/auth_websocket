import { ReactComponent as Error } from "../../assets/icons/error.svg";
import { ReactComponent as Close } from "../../assets/icons/close.svg";
import { ReactComponent as Lock } from "../../assets/icons/lock.svg";

export const iconComponents = {
  error: Error,
  close: Close,
  lock: Lock,
};

export const ICONS = {
  error: "error",
  close: "close",
  lock: "lock",
};
