import React from "react";
import { NotificationsProvider } from "../Notification/Notifications";

export const Providers = ({ children }) => (
  <NotificationsProvider>{children}</NotificationsProvider>
);
