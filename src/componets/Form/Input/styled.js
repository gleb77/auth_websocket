import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  input: {
    width: "100%",
    "&:first-child": {
      marginBottom: "20px",
    },
  },
});
