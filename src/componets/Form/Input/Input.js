import React from "react";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { getIn } from "formik";
import { ErrorMessage } from "../../../componets/ErrorMessage/ErrorMessage";

import { useStyles } from "./styled";

export const Input = ({
  name,
  initialValues,
  disabled,
  placeholder,
  label,
  field: { ...fields },
  shrink,
  infoTitle,
  form: { errors, touched },
  classes: extendedClasses,
  InputProps,
  ErrorMessageComponent = ErrorMessage,
  ...props
}) => {
  const classes = useStyles();
  const errorMessage = getIn(errors, fields.name);
  const isError = errorMessage && getIn(touched, fields.name);

  return (
    <FormControl className={classes.input}>
      <TextField
        {...fields}
        {...props}
        label={label}
        variant="outlined"
        placeholder={placeholder}
        disabled={disabled}
        error={!!isError}
      />

      {isError && <ErrorMessageComponent error={errorMessage} />}
    </FormControl>
  );
};
