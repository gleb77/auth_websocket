import React from "react";
import { Field } from "formik";

export const AutoSaveField = ({ name, ...rest }) => (
  <Field name={name} {...rest} />
);
