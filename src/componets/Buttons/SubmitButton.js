import React, { memo } from "react";
import Button from "@material-ui/core/Button";
import { ReactComponent as Loader } from "./../../assets/icons/loader.svg";

import { useStyles } from "./styled";

export const SubmitButtonBase = ({
  isDisplayLoader,
  label,
  disabled,
  onClick,
}) => {
  const classes = useStyles();

  return (
    <Button
      type="submit"
      fullWidth
      variant="contained"
      color="secondary"
      className={classes.submit}
      disabled={isDisplayLoader || disabled}
      onClick={onClick}
    >
      {isDisplayLoader ? (
        <Loader className={classes.loader} alt="loading" />
      ) : (
        [label]
      )}
    </Button>
  );
};

export const SubmitButton = memo(SubmitButtonBase);
