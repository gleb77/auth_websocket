import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  "@keyframes rotate": {
    "100%": {
      transform: "rotate(360deg)",
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  loader: {
    width: "24px",
    height: "24px",
    margin: "0 auto",
    animation: "$rotate 2s linear infinite",
  },
}));
