export const getRequiredMessage = (fieldName) => `${fieldName} is blank`;

export const getInvalidMessage = (fieldName) => `${fieldName} is invalid`;
