export const ACTIONS_DELIMITER = ",";

export const handleActions = (handlersMap, defaultState) => {
  const keysMap = Object.keys(handlersMap).reduce((accKeysMap, key) => {
    const keyActionsMap = key
      .split(ACTIONS_DELIMITER)
      .reduce(
        (accKeyActionsMap, item) => ({ ...accKeyActionsMap, [item]: key }),
        {}
      );

    return { ...accKeysMap, ...keyActionsMap };
  }, {});

  return (state = defaultState, action) => {
    const actionType = action.type;
    const handler = handlersMap[keysMap[actionType]];

    if (handler) {
      return handler(state, action);
    }

    return state;
  };
};
