import { generatePath } from "react-router";
import { endpoints } from "./../constants/config";

export const buildUrl = (uriName) => {
  const uri = endpoints[uriName];
  return generatePath(uri);
};
